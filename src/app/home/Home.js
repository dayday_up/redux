import React from 'react';
import {Link} from "react-router-dom";

const Home = () => {
  return (
    <div>
      <h3>This is a beautiful home page.</h3>
      <ul>
        <li>
          <Link to='/product-details'>Go to Product Details Page</Link>
        </li>
      </ul>
    </div>
  )
};

export default Home;